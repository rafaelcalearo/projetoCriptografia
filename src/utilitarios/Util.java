package utilitarios;

import java.util.Scanner;

/**
 *
 * @author Rafael
 */
public class Util {

    public static String leString(String mensagem) {
        String msg;

        Scanner in = new Scanner(System.in);
        System.out.print(mensagem);
        msg = in.nextLine();

        return msg;
    }   
}
