package visual;

import modelo.Atbash;
import utilitarios.Util;

public class Tarefa03 {

    public static void main(String[] args) {
        
        String palavraFrase = Util.leString("Digite a palavra/frase a ser criptogragada: ");
        
        Atbash cifrar = new Atbash(palavraFrase);      
        cifrar.cifraPalavraFrase();
        System.out.println(cifrar.palavraFraseCifrada);
    }
}
