![Logo] (https://image.ibb.co/deiZo6/Criptoletras.png) 

# Projeto de Criptografia

O projeto de Criptografia é uma tarefa “03” desenvolvida/começada na 
Aula 01, que faz parte da avaliação da disciplina de Segurança de Sistemas do 
6° semestre da Faculdade de Tecnologia Senac Pelotas – RS e pertence ao 
grupo: [José Lemos](https://gitlab.com/JoseAntonioLemos), 
[Maicon Martins](https://gitlab.com/Maicon) e [Rafael Calearo](https://gitlab.com/rafaelcalearo). 

## O projeto 

Bom, o projeto é desenvolvido na linguagem de programação **JAVA** e foi 
codificado na *IDE* **NetBeans 8.2** e esse se trata de um programa simples que 
usou métodos simples e fáceis de entender (o projeto conta com uma boa 
documentação no código)!

## Objetivo

O objetivo então é implementar um programa simples (lembrando, em JAVA) usando 
algum tipo de método de criptografia ([Cifra de Substituição] (https://pt.wikipedia.org/wiki/Cifra_de_substitui%C3%A7%C3%A3o)): 
**Atbash**, **Cifra de César** ou **ROT13** (que é uma cifra de César) que após 
implementadas no programa pegará uma mensagem digitada pelo usuário – que podem 
ser letras isoladas, uma palavra ou texto – e logo formará a **cifra** 
correspondente ao método escolhido ou fará o inverso retornando uma mensagem 
correspondente a mensagem cifrada digitada. Também foi lançando o desafio de 
cada grupo criar algo diferente dentro do contexto da criptografia e com muito 
café conseguimos desenvolver o método denominado **Block5-JMR**. 

**Mais informações sobre o enunciado do exercício:** 
<https://www.dropbox.com/s/g0920kbwzkfdwy7/2017-08-04%20-%20Aula%2001%20-%20Tarefa_Tres_SS.docx?dl=0>.

### Block5-JMR

O Block5-JMR (nome escolhido pelo número de vezes em que os blocos de cinco 
letras é usado no método de substituição seguido das iniciais dos nomes de seus 
idealizadores – **José Lemos**, **Maicon Martins** e **Rafael Calearo**) foi 
desenvolvido e criado para atender a expectativa de o grupo criar algo diferente 
e o mesmo funciona com a substituição simples do **alfabeto hebraico**. Esse 
método então consiste na substituição da primeira letra “a” pela última letra 
“z”, acompanhado de uma substituição de um grupo de cinco letras por outras 
cinco letras (isso acontece 2x) e por fim a letra “n” por “m”. 

## Mais sobre os métodos

Os métodos mencionados anteriormente podem ser explicados melhor nos *links*:

* **Atbash:** <https://pt.wikipedia.org/wiki/Atbash>; 
* **Cifra de César:** <https://pt.wikipedia.org/wiki/Cifra_de_C%C3%A9sar>;
* **ROT13:** <https://pt.wikipedia.org/wiki/ROT13>.

Para melhor ajudar segue a imagem de todos os métodos com suas substituições e 
o *link* do arquivo para *download*: 

**Arquivo:** <https://www.dropbox.com/s/oiil7l6q59w7zb6/2017-08-04%20-%20Aula%2001%20-%20Tarefa%2003%20-%20Cifras.ods?dl=0>.

**Imagem:**

![Cifras] (https://image.ibb.co/c5AXLb/Substituicoes.png)

## Testando

Bom, para testar se realmente os métodos funcionam ou se o programa retorna as 
mensagens corretamente foi encontrados *sites* a onde foram implantados esses 
métodos que ajudam a comprovar a veracidade dos mesmos:

* **Atbash:** <http://rumkin.com/tools/cipher/atbash.php>;
* **Cifra de César:** <http://www.numaboa.com.br/criptografia/124-substituicao-simples/165-codigo-de-cesar>;
* **ROT13:** <http://www.rot13.com/>;
* **Block5-JMR:** basta fazer o download do projeto junto ao GitLab.