package modelo;

/**
 *
 * @author Rafael
 */
public class Atbash extends Alfabeto {

    public String palavraFrase;
    public String palavraFraseCifrada = "";
    public String palavraFraseDesifrada = "";
    public char[] atbash = {'-', ' ', 'Z', 'Y', 'X', 'W', 'V', 'U', 'T', 'S',
        'R', 'Q', 'P', 'O', 'N', 'M', 'L', 'K', 'J', 'I', 'H', 'G', 'F', 'E',
        'D', 'C', 'B', 'A'};

    public Atbash(String palavraFrase) {
        this.palavraFrase = palavraFrase;
    }

    public void cifraPalavraFrase() {
        int tamanho;

        //CRIA UM ARRAY DAS LETRAS DA PALAVRA/FRASE
        char[] letras = this.palavraFrase.toCharArray();

        //PERCORRE O TAMANHO DO ARRAY DESSAS LETRAS PROVINDAS DA PALAVRA/FRASE
        for (int i = 0; i < letras.length; i++) {
            int j = 0; //REDEFINE O "J" P/ 0 NOVAMENTE
            tamanho = 28; //REDEFINE O TAMANHO NOVAMENTE

            //PERCORRE CADA LETRA DO ALFABETO INCLUIDO O ESPAÇO E O HÍFEN
            while (tamanho > 0) {
                /**
                 * VERIFICA SE A PRIMEIRA LETRA DO ARRAY DE LETRAS É IGUAL A
                 * ALGUMA LETRA DO ARRAY DO ALFABETO
                 */
                if (letras[i] == alfabeto[j]) {
                    /**
                     * SE SIM, AQUI É JOGADO ENTAO A LETRA CORRESPONDENTE LA DO
                     * ARRAY DE LETRAS INVERTIDAS, OU SEJA, DO ALFABETO ATBASH
                     */
                    palavraFraseCifrada += String.valueOf(atbash[j]);
                }
                j++;
                tamanho--;
            }
        }
    }

    public void decifraPalavraFrase() {
        int tamanho;

        //CRIA UM ARRAY DAS LETRAS DA PALAVRA/FRASE
        char[] letras = this.palavraFrase.toCharArray();

        //PERCORRE O TAMANHO DO ARRAY DESSAS LETRAS PROVENIENTES DA PALAVRA/FRASE
        for (int i = 0; i < letras.length; i++) {
            int j = 0; //REDEFINE O "J" P/ 0 NOVAMENTE
            tamanho = 26; //REDEFINE O TAMANHO NOVAMENTE

            //PERCORRE CADA LETRA DO ALFABETO INCLUIDO O ESPAÇO E O HÍFEN
            while (tamanho > 0) {
                /**
                 * VERIFICA SE A PRIMEIRA LETRA DO ARRAY DE LETRAS É IGUAL A
                 * ALGUMA LETRA DO ARRAY DO ALFABETO
                 */
                if (letras[i] == atbash[j]) {
                    /**
                     * SE SIM, AQUI É JOGADO ENTAO A LETRA CORRESPONDENTE LA DO
                     * ARRAY DE LETRAS INVERTIDAS, OU SEJA, DO ALFABETO ATBASH
                     */
                    palavraFraseDesifrada += String.valueOf(alfabeto[j]);
                }
                j++;
                tamanho--;
            }
        }
    }
}
